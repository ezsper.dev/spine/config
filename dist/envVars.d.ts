/**
 * Helper for resolving environment specific configuration files.
 */
/**
 * Gets a string environment variable by the given name.
 *
 * @param  {String} name - The name of the environment variable.
 * @param  {String} defaultVal - The default value to use.
 *
 * @return {String} The value.
 */
export declare function string(name: string, defaultVal: string): string;
/**
 * Gets a number environment variable by the given name.
 *
 * @param  {String} name - The name of the environment variable.
 * @param  {number} defaultVal - The default value to use.
 *
 * @return {number} The value.
 */
export declare function number(name: string, defaultVal: number): number;
export declare function bool(name: string, defaultVal: boolean): boolean;
