"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterWithRules = filterWithRules;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.object.keys");

function filterWithRulesLoop(rules, obj) {
  var basePropPath = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
  return Object.keys(rules).reduce(function (acc, key) {
    var propPath = basePropPath !== '' ? "".concat(basePropPath, ".").concat(key) : key;

    if ((0, _typeof2.default)(rules[key]) === 'object') {
      if (obj[key] == null) {
        return obj[key];
      }

      if ((0, _typeof2.default)(obj[key]) !== 'object') {
        throw new Error("Expected prop at path \"".concat(propPath, "\" to be an object"));
      }

      if (Array.isArray(obj[key])) {
        var arr = [];

        for (var i in obj[key]) {
          arr.push(filterWithRulesLoop(rules[key], obj[key][i], "".concat(propPath, ".").concat(i)));
        }

        acc[key] = arr;
      } else {
        acc[key] = filterWithRulesLoop(rules[key], obj[key], propPath);
      }
    } else if (rules[key] != null) {
      /*
       * TODO: Revise, I don't think we should keep warning this in favor of
       * optional configuration
       */

      /*
      warning(
        typeof obj[key] !== 'undefined',
        `Filter set an "allow" on path "${propPath}", however, this path was not found on the source object.`
      );
      */
      acc[key] = obj[key]; // eslint-disable-line no-param-reassign
    }

    return acc;
  }, {});
}
/**
 * Applies a rules object to filter a given object's structure.
 *
 * The rules object should match the shape of the source object and should
 * have a truthy/falsey value indicating if a property should be included/
 * excluded.  If the filters do not contain a property that exists on the
 * source object then the respective property will be excluded.
 *
 * @param  {Object} rules : The filter rules.
 * @param  {Object} obj   : The object to filter.
 *
 * @return {Object}
 *   The filtered object.
 *
 * @example
 *   filter(
 *     // rules
 *     {
 *       foo: { bar: true },
 *       poop: true
 *     },
 *     // source
 *     {
 *       foo: { bar: 'bar', qux: 'qux' },
 *       bob: 'bob',
 *       poop: { plop: 'splash' }
 *     },
 *   )
 */


function filterWithRules(rules, obj) {
  return filterWithRulesLoop(rules, obj);
}
//# sourceMappingURL=filterWithRules.js.map