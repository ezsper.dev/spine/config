/**
 * Unified Configuration Reader
 *
 * This helper function allows you to use the same API in accessing configuration
 * values no matter where the code is being executed (i.e. browser/node).
 *
 * e.g.
 *   import config from '../config';
 *   config('welcomeMessage'); // => "Hello World!"
 *   cinfig.welcomeMessage // => "Hello World!"
 */
declare type Unaoptional<T> = Exclude<T, undefined | null | void>;
declare type Unpacked<T> = T extends (infer U)[] ? U : T;
export declare type ConfigFilter<T = any> = {
    [K in keyof T]?: Unpacked<Unaoptional<T[K]>> extends Function ? boolean : (Unpacked<Unaoptional<T[K]>> extends {
        [key: string]: any;
    } ? boolean | ConfigFilter<Unpacked<Unaoptional<T[K]>>> : boolean);
};
export interface BaseConfigSource {
}
export declare type Immutable<T> = T extends Function ? T : (T extends (infer U)[] ? ReadonlyArray<U> : (T extends {
    [key: string]: any;
} ? {
    readonly [P in keyof T]: Immutable<T[P]>;
} : T));
/**
 * This function wraps up the boilerplate needed to access the correct
 * configuration depending on whether your code will get executed in the
 * browser/node.
 *
 * i.e.
 *  - For the browser the config values are available at global.__CONFIG__
 *  - For a node process they are within the "<root>/config".
 *
 * To request a configuration value you must provide the repective path. For
 * example, f you had the following configuration structure:
 *   {
 *     foo: {
 *       bar: [1, 2, 3]
 *     },
 *     bob: 'bob'
 *   }
 *
 * You could use this function to access "bar" like so:
 *   import config from '../config';
 *   const value = config('foo.bar');
 *
 * And you could access "bob" like so:
 *   import config from '../config';
 *   const value = config('bob');
 *
 * If any part of the path isn't available as a configuration key/value then
 * an error will be thrown indicating that a respective configuration value
 * could not be found at the given path.
 */
export interface ConfigCallable<T extends BaseConfigSource> {
    <T = any>(path: PropertyKey | PropertyKey[]): T | void;
    <T = any>(path: PropertyKey | PropertyKey[], defaultValue: T): T;
    namespace: string;
    export(): T;
    load(input: T): void;
    extend<E extends T>(): Config<E>;
}
export interface ConfigCallableWithHydration<T extends BaseConfigSource> extends ConfigCallable<T> {
    hydrationKey: string;
    hydrate(): void;
    extend<E extends T>(): ConfigWithHydration<E>;
}
export declare type Config<T extends BaseConfigSource> = ConfigCallable<T> & T & Readonly<ConfigCallable<T>>;
export declare type ConfigWithHydration<T extends BaseConfigSource> = ConfigCallableWithHydration<T> & T & Readonly<ConfigCallableWithHydration<T>>;
export declare function getConfig<T extends BaseConfigSource>(namespace: string): Config<T> | ConfigWithHydration<T>;
export interface CreateConfig {
    <T extends BaseConfigSource>(namespace: string): Config<T>;
    <T extends BaseConfigSource>(namespace: string, hydrationKey: string): ConfigWithHydration<T>;
}
export declare const createConfig: CreateConfig;
export declare function getNamespaces(): string[];
export {};
