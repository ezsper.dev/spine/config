"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getConfig = getConfig;
exports.getNamespaces = getNamespaces;
exports.createConfig = void 0;

require("core-js/modules/es6.reflect.own-keys");

require("core-js/modules/es7.symbol.async-iterator");

require("core-js/modules/es6.symbol");

require("core-js/modules/web.dom.iterable");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.object.keys");

require("core-js/modules/es6.object.assign");

require("core-js/modules/es6.regexp.split");

require("core-js/modules/es6.regexp.to-string");

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

/**
 * Unified Configuration Reader
 *
 * This helper function allows you to use the same API in accessing configuration
 * values no matter where the code is being executed (i.e. browser/node).
 *
 * e.g.
 *   import config from '../config';
 *   config('welcomeMessage'); // => "Hello World!"
 *   cinfig.welcomeMessage // => "Hello World!"
 */
// PRIVATES
// EXPORT

/**
 * This function wraps up the boilerplate needed to access the correct
 * configuration depending on whether your code will get executed in the
 * browser/node.
 *
 * i.e.
 *  - For the browser the config values are available at global.__CONFIG__
 *  - For a node process they are within the "<root>/config".
 *
 * To request a configuration value you must provide the repective path. For
 * example, f you had the following configuration structure:
 *   {
 *     foo: {
 *       bar: [1, 2, 3]
 *     },
 *     bob: 'bob'
 *   }
 *
 * You could use this function to access "bar" like so:
 *   import config from '../config';
 *   const value = config('foo.bar');
 *
 * And you could access "bob" like so:
 *   import config from '../config';
 *   const value = config('bob');
 *
 * If any part of the path isn't available as a configuration key/value then
 * an error will be thrown indicating that a respective configuration value
 * could not be found at the given path.
 */
var configSource = {};
var namespaces = {};

function getConfig(namespace) {
  if (namespaces[namespace] === undefined) {
    throw new Error("No config namespace \"".concat(namespace, "\" defined"));
  }

  return namespaces[namespace];
}

function makeArray(value) {
  if (Array.isArray(value)) {
    return value;
  }

  return [value];
}

function keyStringfy(value) {
  if ((0, _typeof2.default)(value) === 'symbol') {
    return value.toString();
  }

  return "".concat(value);
}

function pathStringfy(path) {
  return path.map(keyStringfy).join('.');
}

function configPathError(parts, namespace) {
  var errorMessage = "Failed to resolve configuration value at \"".concat(pathStringfy(parts), "\"."); // This "if" block gets stripped away by webpack for production builds.

  if (loaded[namespace] != null && loaded[namespace].hydrated) {
    return new Error("".concat(errorMessage, " We have noticed that you are trying to access this configuration value from hydrated config (i.e. code that will potentially be executed publicly). For configuration values to be exposed to the public you must ensure that the path is added to the configuration filter you are using."));
  }

  return new Error(errorMessage);
}

var createConfig = function createConfig(namespace, hydrationKey) {
  if (namespace in namespaces) {
    throw new Error("Namespace \"".concat(namespace, "\" already created"));
  }

  var config = function config(path, defaultValue) {
    var parts = typeof path === 'string' ? path.split('.') : makeArray(path);

    if (parts.length === 0) {
      throw new Error('You must provide the path to the configuration value you would like to consume.');
    }

    if (configSource[namespace] === undefined) {
      throw new Error('Config wasn\'t loaded');
    }

    var result = configSource[namespace];

    for (var i = 0; i < parts.length; i += 1) {
      if (result === undefined) {
        throw configPathError(parts.slice(0, i), namespace);
      }

      result = result[parts[i]];
    }

    if (result === undefined) {
      if (typeof defaultValue === 'undefined') {
        throw configPathError(parts, namespace);
      }

      return defaultValue;
    }

    return result;
  };

  namespaces[namespace] = Object.assign(config, {
    namespace: namespace,
    hydrationKey: hydrationKey,
    load: function load(input) {
      return _load(namespace, input);
    },
    hydrate: function hydrate() {
      return _hydrate(namespace);
    },
    export: function _export() {
      return exportConfig(namespace);
    },
    extend: function extend() {
      if (loaded[namespace]) {
        throw new Error('Cannot extend if already loaded');
      }

      return namespaces[namespace];
    }
  });
  assignConfig(namespace, undefined);
  return namespaces[namespace];
};

exports.createConfig = createConfig;

function getNamespaces() {
  return Object.keys(namespaces);
}

function exportConfig(namespace) {
  return configSource[namespace];
}

function isObjectLike(value) {
  return (0, _typeof2.default)(value) === 'object' && !Array.isArray(value);
}

function assignConfig(namespace, values) {
  // i.e. running in our server/node process.
  // eslint-disable-next-line global-require
  if (values !== undefined) {
    configSource[namespace] = Object.assign({}, values);
  }

  var config = namespaces[namespace];

  if ((typeof Proxy === "undefined" ? "undefined" : (0, _typeof2.default)(Proxy)) !== undefined) {
    var skipField = function skipField(field) {
      if ((0, _typeof2.default)(field) === 'symbol') {
        if (skipSymbols.indexOf(field) >= 0) {
          return true;
        }
      }

      return skipFields.indexOf(field.toString()) >= 0;
    };

    var emptyObject = function emptyObject(obj) {
      if (obj === undefined) {
        return {};
      }

      return obj;
    };

    var proxyConfigObject = function proxyConfigObject(source) {
      var path = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      return new Proxy(source, {
        get: function get(target, field) {
          if (skipField(field)) {
            return emptyObject(source)[field];
          }

          var relativePath = path.concat(field);

          if (source[field] !== undefined) {
            if (isObjectLike(source[field])) {
              return proxyConfigObject(source[field], relativePath);
            }
          }

          return namespaces[namespace](relativePath);
        },
        getOwnPropertyDescriptor: function getOwnPropertyDescriptor(target, field) {
          return Object.getOwnPropertyDescriptor(emptyObject(source), field);
        },
        enumerate: function enumerate() {
          return emptyObject(source)[Symbol.iterator]();
        },
        ownKeys: function ownKeys() {
          return Reflect.ownKeys(emptyObject(source)).concat(['prototype']);
        },
        has: function has(target, prop) {
          return source !== undefined && prop in source;
        }
      });
    };

    var proxyConfig = function proxyConfig(source, proxyValue) {
      return new Proxy(source, {
        get: function get(target, field) {
          if (field in source) {
            return source[field];
          }

          if (skipField(field)) {
            return emptyObject(proxyValue)[field];
          }

          if (proxyValue !== undefined) {
            if (isObjectLike(proxyValue[field])) {
              return proxyConfigObject(proxyValue[field], [field]);
            }
          }

          return namespaces[namespace]([field]);
        },
        getOwnPropertyDescriptor: function getOwnPropertyDescriptor(target, field) {
          if (field in source) {
            return Object.getOwnPropertyDescriptor(target, field);
          }

          return Object.getOwnPropertyDescriptor(emptyObject(proxyValue), field);
        },
        enumerate: function enumerate() {
          return emptyObject(proxyValue)[Symbol.iterator]();
        },
        ownKeys: function ownKeys() {
          return Reflect.ownKeys(emptyObject(source)).concat(Reflect.ownKeys(emptyObject(proxyValue))).concat(['prototype']);
        },
        has: function has(target, prop) {
          return prop in source || proxyValue !== undefined && prop in proxyValue;
        }
      });
    };

    var skipSymbols = [Symbol.toStringTag];
    var skipFields = ['Symbol(util.inspect.custom)', // node
    'inspect', 'displayName', 'name', 'prototype', '__proto__'];
    namespaces[namespace] = proxyConfig(config, configSource[namespace]);
  } else {
    namespaces[namespace] = Object.assign(config, configSource[namespace], config);
  }
}

var loaded = {};

function _hydrate(namespace) {
  if (loaded[namespace] != null) {
    throw new Error('Config was already loaded');
  }

  var hydrationKey = namespaces[namespace].hydrationKey;

  if (typeof hydrationKey === 'undefined') {
    throw new Error('No config hydration key was set');
  }

  if ((0, _typeof2.default)(global[hydrationKey]) === 'object') {
    var data = global[hydrationKey];
    loaded[namespace] = {
      hydrated: true
    };
    assignConfig(namespace, Object.assign({}, data));
  } else {
    throw new Error("global.\"".concat(hydrationKey, "\" is not defined"));
  }
}

function _load(namespace, input) {
  if (loaded[namespace]) {
    throw new Error('Your app\'s config was already loaded');
  }

  loaded[namespace] = {
    hydrated: false
  };
  assignConfig(namespace, input);
}
//# sourceMappingURL=index.js.map