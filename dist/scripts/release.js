"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var appRootDir = _interopRequireWildcard(require("app-root-dir"));

var _path = require("path");

var fs = _interopRequireWildcard(require("fs-extra"));

var _exec = require("../internal/utils/exec");

/**
 * This script builds to distribuition
 */
// First clear the build output dir.
fs.removeSync((0, _path.resolve)(appRootDir.get(), './dist')); // Ignore all that is not used on client

var ignore = ["node_modules", "bin", "build", "dist", "tests", "types.d", "sample"].join(','); // Then compile to pure JS

(0, _exec.exec)("tsc --emitDeclarationOnly --skipLibCheck && babel --source-maps --ignore ".concat(ignore, " -d dist --extensions \".ts,.tsx\" .")); // Copy documents

fs.copySync((0, _path.resolve)(appRootDir.get(), 'README.md'), (0, _path.resolve)(appRootDir.get(), 'dist/README.md')); // Clear our package json for release

var packageJson = require('../package.json');

packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson.private;
delete packageJson['lint-staged'];
delete packageJson.jest;
delete packageJson.devDependencies;
fs.writeJsonSync((0, _path.resolve)(appRootDir.get(), './dist/package.json'), packageJson, {
  spaces: 2
});
fs.removeSync((0, _path.resolve)(appRootDir.get(), './dist/internal'));
fs.copySync((0, _path.resolve)(appRootDir.get(), 'LICENSE'), (0, _path.resolve)(appRootDir.get(), 'dist/LICENSE'));
//# sourceMappingURL=release.js.map