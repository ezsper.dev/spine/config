import { BaseConfigSource, ConfigWithHydration, ConfigFilter } from '.';
export * from '.';
export declare function exportClientConfig<T extends BaseConfigSource>(config: ConfigWithHydration<T>, configFilter: ConfigFilter<T>): any;
export declare function createVariableInjection(key: string, obj: {}): string;
export declare function encodeJavascriptToSource(code: string): string;
export declare function renderInjection(source: string, nonce?: string): string;
export declare function getPublicConfigInjection<T extends BaseConfigSource>(config: ConfigWithHydration<T>, configFilter: ConfigFilter<T>): string;
export declare function renderClientConfig<T extends BaseConfigSource>(config: ConfigWithHydration<T>, configFilter: ConfigFilter<T>, nonce?: string): string;
