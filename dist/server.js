"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/es6.array.iterator");

require("core-js/modules/es6.object.keys");

require("core-js/modules/web.dom.iterable");

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  exportClientConfig: true,
  createVariableInjection: true,
  encodeJavascriptToSource: true,
  renderInjection: true,
  getPublicConfigInjection: true,
  renderClientConfig: true
};
exports.exportClientConfig = exportClientConfig;
exports.createVariableInjection = createVariableInjection;
exports.encodeJavascriptToSource = encodeJavascriptToSource;
exports.renderInjection = renderInjection;
exports.getPublicConfigInjection = getPublicConfigInjection;
exports.renderClientConfig = renderClientConfig;

require("core-js/modules/es6.regexp.to-string");

var _serializeJavascript = _interopRequireDefault(require("serialize-javascript"));

var _filterWithRules = require("./filterWithRules");

var _ = require(".");

Object.keys(_).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _[key];
    }
  });
});

function exportClientConfig(config, configFilter) {
  return (0, _filterWithRules.filterWithRules)(configFilter, config.export());
}

function createVariableInjection(key, obj) {
  var options;

  if (process.env.NODE_ENV !== 'production') {
    options = {
      space: 2
    };
  }

  var serialized = (0, _serializeJavascript.default)(obj, options);
  return "window.".concat(key, "=").concat(serialized);
}

function encodeJavascriptToSource(code) {
  return "data:text/javascript;base64,".concat(new Buffer(code).toString('base64'));
}

function renderInjection(source, nonce) {
  var attributes = typeof nonce !== 'undefined' ? " nonce=\"".concat(nonce, "\"") : '';

  if (process.env.NODE_ENV !== 'production') {
    return "<script".concat(attributes, ">\n").concat(source, "\n</script>");
  }

  return "<script src=\"".concat(encodeJavascriptToSource(source), "\"").concat(attributes, "></script>");
}

function getPublicConfigInjection(config, configFilter) {
  if (typeof config.hydrationKey === 'undefined') {
    throw new Error('No hydration key was set');
  }

  return createVariableInjection(config.hydrationKey, exportClientConfig(config, configFilter));
}

function renderClientConfig(config, configFilter, nonce) {
  return renderInjection(getPublicConfigInjection(config, configFilter), nonce);
}
//# sourceMappingURL=server.js.map