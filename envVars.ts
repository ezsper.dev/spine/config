/**
 * Helper for resolving environment specific configuration files.
 */

// EXPORTED HELPERS

/**
 * Gets a string environment variable by the given name.
 *
 * @param  {String} name - The name of the environment variable.
 * @param  {String} defaultVal - The default value to use.
 *
 * @return {String} The value.
 */
export function string(name: string, defaultVal: string): string {
  return process.env[name] || defaultVal;
}

/**
 * Gets a number environment variable by the given name.
 *
 * @param  {String} name - The name of the environment variable.
 * @param  {number} defaultVal - The default value to use.
 *
 * @return {number} The value.
 */
export function number(name: string, defaultVal: number): number {
  return process.env[name]
    ? parseInt(process.env[name] as string, 10)
    : defaultVal;
}

export function bool(name: string, defaultVal: boolean): boolean {
  return process.env[name]
    ? process.env[name] === 'true' || process.env[name] === '1'
    : defaultVal;
}
