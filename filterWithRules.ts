export type Rules = { [key: string]: boolean | Rules | undefined };

function filterWithRulesLoop(
  rules: Rules,
  obj: { [key: string]: any },
  basePropPath = ''
) {
  return Object.keys(rules).reduce((acc: any, key: string) => {
    const propPath = basePropPath !== '' ? `${basePropPath}.${key}` : key;

    if (typeof rules[key] === 'object') {
      if (obj[key] == null) {
        return obj[key];
      }
      if (typeof obj[key] !== 'object') {
        throw new Error(`Expected prop at path "${propPath}" to be an object`);
      }
      if (Array.isArray(obj[key])) {
        const arr = [];
        for (const i in obj[key]) {
          arr.push(filterWithRulesLoop(rules[key] as Rules, obj[key][i], `${propPath}.${i}`));
        }
        acc[key] = arr;
      } else {
        acc[key] = filterWithRulesLoop(rules[key] as Rules, obj[key], propPath);
      }
    } else if (rules[key] != null) {
      /*
       * TODO: Revise, I don't think we should keep warning this in favor of
       * optional configuration
       */
      /*
      warning(
        typeof obj[key] !== 'undefined',
        `Filter set an "allow" on path "${propPath}", however, this path was not found on the source object.`
      );
      */
      acc[key] = obj[key]; // eslint-disable-line no-param-reassign
    }
    return acc;
  }, {});
}

/**
 * Applies a rules object to filter a given object's structure.
 *
 * The rules object should match the shape of the source object and should
 * have a truthy/falsey value indicating if a property should be included/
 * excluded.  If the filters do not contain a property that exists on the
 * source object then the respective property will be excluded.
 *
 * @param  {Object} rules : The filter rules.
 * @param  {Object} obj   : The object to filter.
 *
 * @return {Object}
 *   The filtered object.
 *
 * @example
 *   filter(
 *     // rules
 *     {
 *       foo: { bar: true },
 *       poop: true
 *     },
 *     // source
 *     {
 *       foo: { bar: 'bar', qux: 'qux' },
 *       bob: 'bob',
 *       poop: { plop: 'splash' }
 *     },
 *   )
 */
export function filterWithRules(rules: Rules, obj: { [key: string]: any }) {
  return filterWithRulesLoop(rules, obj);
}
