/**
 * Unified Configuration Reader
 *
 * This helper function allows you to use the same API in accessing configuration
 * values no matter where the code is being executed (i.e. browser/node).
 *
 * e.g.
 *   import config from '../config';
 *   config('welcomeMessage'); // => "Hello World!"
 *   cinfig.welcomeMessage // => "Hello World!"
 */

// PRIVATES

type Unaoptional<T> = Exclude<T, undefined | null | void>;
type Unpacked<T> = T extends (infer U)[] ? U : T;

export type ConfigFilter<T = any> = {
  [K in keyof T]?: Unpacked<Unaoptional<T[K]>> extends Function
    ? boolean
    : (Unpacked<Unaoptional<T[K]>> extends { [key: string]: any } ? boolean | ConfigFilter<Unpacked<Unaoptional<T[K]>>> : boolean);
};

export interface BaseConfigSource {}


export type Immutable<T> = T extends Function
  ? T
  : (T extends (infer U)[]
    ? ReadonlyArray<U>
    : ( T extends { [key: string]: any }
      ? { readonly [P in keyof T]: Immutable<T[P]> }
      : T
      )
    );

// EXPORT

/**
 * This function wraps up the boilerplate needed to access the correct
 * configuration depending on whether your code will get executed in the
 * browser/node.
 *
 * i.e.
 *  - For the browser the config values are available at global.__CONFIG__
 *  - For a node process they are within the "<root>/config".
 *
 * To request a configuration value you must provide the repective path. For
 * example, f you had the following configuration structure:
 *   {
 *     foo: {
 *       bar: [1, 2, 3]
 *     },
 *     bob: 'bob'
 *   }
 *
 * You could use this function to access "bar" like so:
 *   import config from '../config';
 *   const value = config('foo.bar');
 *
 * And you could access "bob" like so:
 *   import config from '../config';
 *   const value = config('bob');
 *
 * If any part of the path isn't available as a configuration key/value then
 * an error will be thrown indicating that a respective configuration value
 * could not be found at the given path.
 */
export interface ConfigCallable<T extends BaseConfigSource> {
  <T = any>(path: PropertyKey | PropertyKey[]): T | void;
  <T = any>(path: PropertyKey | PropertyKey[], defaultValue: T): T;
  namespace: string;
  export(): T;
  load(input: T): void;
  extend<E extends T>(): Config<E>;
}

export interface ConfigCallableWithHydration<T extends BaseConfigSource> extends ConfigCallable<T> {
  hydrationKey: string;
  hydrate(): void;
  extend<E extends T>(): ConfigWithHydration<E>;
}

export type Config<T extends BaseConfigSource> = ConfigCallable<T> & T & Readonly<ConfigCallable<T>>;
export type ConfigWithHydration<T extends BaseConfigSource> = ConfigCallableWithHydration<T> & T & Readonly<ConfigCallableWithHydration<T>>;

const configSource: {[namespace: string]: BaseConfigSource} = {};
const namespaces: {[namespace: string]: Config<BaseConfigSource>} = {};

export function getConfig<T extends BaseConfigSource>(namespace: string): Config<T> | ConfigWithHydration<T> {
  if (namespaces[namespace] === undefined) {
    throw new Error(`No config namespace "${namespace}" defined`);
  }
  return <any>namespaces[namespace];
}

function makeArray(value: any): any[] {
  if (Array.isArray(value)) {
    return value;
  }
  return [value];
}

function keyStringfy(value: PropertyKey) {
  if (typeof value === 'symbol') {
    return value.toString();
  }
  return `${value}`;
}
function pathStringfy(path: PropertyKey[]) {
  return path.map(keyStringfy).join('.');
}

function configPathError(parts: PropertyKey[], namespace: string) {
  const errorMessage = `Failed to resolve configuration value at "${pathStringfy(parts)}".`;
  // This "if" block gets stripped away by webpack for production builds.
  if (loaded[namespace] != null && loaded[namespace].hydrated) {
    return new Error(
      `${errorMessage} We have noticed that you are trying to access this configuration value from hydrated config (i.e. code that will potentially be executed publicly). For configuration values to be exposed to the public you must ensure that the path is added to the configuration filter you are using.`
    );
  }
  return new Error(errorMessage);
}

export interface CreateConfig {
  <T extends BaseConfigSource>(namespace: string): Config<T>;
  <T extends BaseConfigSource>(namespace: string, hydrationKey: string): ConfigWithHydration<T>;
}

export const createConfig: CreateConfig = (namespace: string, hydrationKey?: string) => {
  if (namespace in namespaces) {
    throw new Error(`Namespace "${namespace}" already created`);
  }

  const config = <any>((path: string, defaultValue?: any) => {
    const parts = <PropertyKey[]>(typeof path === 'string' ? path.split('.') : makeArray(path));

    if (parts.length === 0) {
      throw new Error(
        'You must provide the path to the configuration value you would like to consume.',
      );
    }

    if (configSource[namespace] === undefined) {
      throw new Error(
        'Config wasn\'t loaded',
      );
    }

    let result: any = configSource[namespace];
    for (let i = 0; i < parts.length; i += 1) {
      if (result === undefined) {
        throw configPathError(parts.slice(0, i), namespace);
      }
      result = result[parts[i]];
    }
    if (result === undefined) {
      if (typeof defaultValue === 'undefined') {
        throw configPathError(parts, namespace);
      }
      return defaultValue;
    }
    return result;
  });

  namespaces[namespace] = <any>Object.assign(config, {
    namespace,
    hydrationKey,
    load: (input: any) => load(namespace, input),
    hydrate: () => hydrate(namespace),
    export: () => exportConfig(namespace),
    extend: () => {
      if (loaded[namespace]) {
        throw new Error('Cannot extend if already loaded');
      }
      return namespaces[namespace];
    },
  });

  assignConfig(namespace, undefined);

  return <any>namespaces[namespace];
};

export function getNamespaces() {
  return Object.keys(namespaces);
}

function exportConfig<T extends BaseConfigSource = BaseConfigSource>(namespace: string): T {
  return <any>configSource[namespace];
}

function isObjectLike(value: any) {
  return typeof value === 'object'
    && !Array.isArray(value);
}

function assignConfig<T extends BaseConfigSource = BaseConfigSource>(namespace: string, values: T | void) {
  // i.e. running in our server/node process.
  // eslint-disable-next-line global-require
  if (values !== undefined) {
    configSource[namespace] = Object.assign({}, values);
  }

  const config = namespaces[namespace];

  if (typeof Proxy !== undefined) {
    const skipSymbols = [
      Symbol.toStringTag,
    ];

    const skipFields = [
      'Symbol(util.inspect.custom)', // node
      'inspect',
      'displayName',
      'name',
      'prototype',
      '__proto__',
    ];

    function skipField(field: PropertyKey): boolean {
      if (typeof field === 'symbol') {
        if (skipSymbols.indexOf(field) >= 0) {
          return true;
        }
      }
      return skipFields.indexOf(field.toString()) >= 0;
    }

    function emptyObject(obj: {} | void): any {
      if (obj === undefined) {
        return {};
      }
      return obj;
    }

    function proxyConfigObject(source: any, path: PropertyKey[] = []): any {
      return new Proxy(source, {
        get(target, field) {
          if (skipField(field)) {
            return emptyObject(source)[field];
          }
          const relativePath = path.concat(field);
          if (source[field] !== undefined) {
            if (isObjectLike(source[field])) {
              return proxyConfigObject(source[field], relativePath);
            }
          }
          return (<any>namespaces[namespace])(relativePath);
        },
        getOwnPropertyDescriptor(target, field) {
          return Object.getOwnPropertyDescriptor(emptyObject(source), field);
        },
        enumerate() {
          return emptyObject(source)[Symbol.iterator]();
        },
        ownKeys() {
          return Reflect.ownKeys(emptyObject(source)).concat(['prototype']);
        },
        has(target, prop) {
          return source !== undefined && prop in source;
        },
      });
    }

    function proxyConfig(
      source: any,
      proxyValue: any,
    ): {[key: string]: any} {
      return new Proxy(source, {
        get(target, field) {
          if (field in source) {
            return source[field];
          }
          if (skipField(field)) {
            return emptyObject(proxyValue)[field];
          }
          if (proxyValue !== undefined) {
            if (isObjectLike(proxyValue[field])) {
              return proxyConfigObject(proxyValue[field], [field]);
            }
          }
          return (<any>namespaces[namespace])([field]);
        },
        getOwnPropertyDescriptor(target, field) {
          if (field in source) {
            return Object.getOwnPropertyDescriptor(target, field);
          }
          return Object.getOwnPropertyDescriptor(emptyObject(proxyValue), field);
        },
        enumerate() {
          return emptyObject(proxyValue)[Symbol.iterator]();
        },
        ownKeys() {
          return Reflect.ownKeys(emptyObject(source))
            .concat(Reflect.ownKeys(emptyObject(proxyValue)))
            .concat(['prototype']);
        },
        has(target, prop) {
          return prop in source || (proxyValue !== undefined && prop in proxyValue);
        },
      });
    }
    namespaces[namespace] = <any>proxyConfig(config, configSource[namespace]);
  } else {
    namespaces[namespace] = <any>Object.assign(config, configSource[namespace], config);
  }
}

const loaded: {[namespace: string]: { hydrated: boolean }} = {};

function hydrate(namespace: string) {
  if (loaded[namespace] != null) {
    throw new Error('Config was already loaded');
  }
  const { hydrationKey } = (<any>namespaces[namespace]);
  if (typeof hydrationKey === 'undefined') {
    throw new Error('No config hydration key was set');
  }
  if (typeof (<any>global)[hydrationKey] === 'object') {
    const data = (<any>global)[hydrationKey];
    loaded[namespace] = { hydrated: true };
    assignConfig(namespace, Object.assign({}, data));
  } else {
    throw new Error(`global."${hydrationKey}" is not defined`);
  }
}

function load<T extends BaseConfigSource>(namespace: string, input: T) {
  if (loaded[namespace]) {
    throw new Error('Your app\'s config was already loaded');
  }
  loaded[namespace] = { hydrated: false };
  assignConfig<T>(namespace, input);
}