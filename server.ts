import serializeJavascript from 'serialize-javascript';
import {
  BaseConfigSource,
  ConfigWithHydration,
  ConfigFilter,
} from '.';
import { filterWithRules } from './filterWithRules';

export * from '.';

export function exportClientConfig<T extends BaseConfigSource>(config: ConfigWithHydration<T>, configFilter: ConfigFilter<T>): any {
  return filterWithRules(configFilter, config.export());
}

export function createVariableInjection(key: string, obj: {}) {
  let options;
  if (process.env.NODE_ENV !== 'production') {
    options = { space: 2 };
  }
  const serialized = serializeJavascript(obj, options);
  return `window.${key}=${serialized}`;
}

export function encodeJavascriptToSource(code: string) {
  return `data:text/javascript;base64,${(new Buffer(code)).toString('base64')}`
}

export function renderInjection(source: string, nonce?: string) {
  const attributes = typeof nonce !== 'undefined' ? ` nonce="${nonce}"` : '';

  if (process.env.NODE_ENV !== 'production') {
    return `<script${attributes}>\n${source}\n</script>`;
  }

  return `<script src="${encodeJavascriptToSource(source)}"${attributes}></script>`;
}

export function getPublicConfigInjection<T extends BaseConfigSource>(config: ConfigWithHydration<T>, configFilter: ConfigFilter<T>) {
  if (typeof config.hydrationKey === 'undefined') {
    throw new Error('No hydration key was set');
  }
  return createVariableInjection(config.hydrationKey, exportClientConfig(config, configFilter));
}

export function renderClientConfig<T extends BaseConfigSource>(config: ConfigWithHydration<T>, configFilter: ConfigFilter<T>, nonce?: string) {
  return renderInjection(getPublicConfigInjection(config, configFilter), nonce);
}
