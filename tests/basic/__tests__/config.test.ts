import * as path from 'path';
declare var jest, describe, it, expect;

describe('Config', () => {

  it('config.htmlPage should be defined', async () => {
    const { config } = await import('../src/config');
    expect(config.htmlPage).toBeDefined();
    expect(config.htmlPage.welcomeMessage).toBe('Hello World');
    expect(config('htmlPage.welcomeMessage')).toBe('Hello World');
    expect(config.htmlPage.secret).toBe('My Secret');
  });

  it('Client config export', async () => {
    const { config } = await import('../src/config');
    const { exportClientConfig } = await import('../../../server');
    const clientConfig = exportClientConfig(config, config.clientConfigFilter);
    expect(clientConfig).toBeDefined();
    expect(clientConfig.htmlPage).toBeDefined();
    expect(clientConfig.htmlPage.welcomeMessage).toBe('Hello World');
    expect(clientConfig.htmlPage.secret).toBeUndefined();
    expect(clientConfig.listOfObject).toBeDefined();
    expect(clientConfig.listOfObject[0]).toBeDefined();
    expect(clientConfig.listOfObject[0].label).toBe('foo');
  });

});
