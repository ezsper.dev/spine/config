import { ConfigSource } from '.';

export const clientConfigFilter: ConfigSource['clientConfigFilter'] = {
  htmlPage: {
    welcomeMessage: true,
  },
  listOfObject: {
    label: true,
    anotherOption: true,
  },
};
