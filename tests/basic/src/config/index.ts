import {
  createConfig,
  ConfigFilter,
} from '../../../..';

export { ConfigFilter };

export interface MyConfigSource {
  clientConfigFilter: ConfigFilter<this>;
  server: {
    address: string;
    port: number;
  },
  htmlPage: {
    welcomeMessage: string;
    secret: string;
  },
  listOfObject: {
    label: string;
    anotherOption: string;
  }[];
}

export interface ConfigSource extends MyConfigSource {}
export const config = createConfig<ConfigSource>('test', '__CONFIG__');

if (typeof window !== 'undefined') {
  config.hydrate();
} else {
  require('./load');
}

export default config;
