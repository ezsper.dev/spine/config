import { config } from '.';
import { clientConfigFilter } from './clientConfigFilter';

config.load({
  clientConfigFilter,
  server: {
    address: '0.0.0.0',
    port: 3000,
  },
  htmlPage: {
    welcomeMessage: 'Hello World',
    secret: 'My Secret',
  },
  listOfObject: [
    {
      label: 'foo',
      anotherOption: 'bar',
    },
  ],
});
